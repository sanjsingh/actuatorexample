package org.example.actuator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.stereotype.Component;

@Component
@Endpoint(id="stage")
public class StageEnd {

	Map<String, Stage> stages = new ConcurrentHashMap<>();

	@ReadOperation
	public Map<String, Stage> getAllStages() {
		return stages;

	}

	@ReadOperation
	public Stage getStages(@Selector String name) {
		return stages.get(name);
	}

	@WriteOperation
	public Stage setStages(@Selector String name, Stage stage) {
		return stages.put(name, stage);

	}

	static class Stage {

		private int id;

		public Stage(int id) {
			super();
			this.id = id;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

	}

}
